<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Atomica\Bluestone\Contracts\Engine;
use Atomica\Bluestone\Engines\Php;
use Interop\Container\ContainerInterface;

require join(DIRECTORY_SEPARATOR, [__DIR__, '..', 'vendor', 'autoload.php']);

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions([
    Engine::class => function (ContainerInterface $container) {
        return new Php($container);
    },
]);
$container = $builder->build();
$container->set(ContainerInterface::class, $container);

$engine = $container->get(Engine::class);
echo $engine->render(\Example\Homepage\Views\Index::class, ['title' => 'this is the title']);
