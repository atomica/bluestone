<?php

declare(strict_types = 1);

namespace Example\Fixtures;

use Atomica\Bluestone\Contracts\View;

class MissingTemplate implements View
{
    public function prepare(array $hints = []) : array
    {
        return $hints;
    }
}
