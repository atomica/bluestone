<?php
namespace Example\Images\Views;

use Atomica\Bluestone\Contracts\View;

class Placeholder implements View
{
    private $sizes = [1, 2, 2.5, 3, 4];


    public function prepare(array $hints = []) : array
    {
        $width = $hints['width'] ?? 128;
        $height = $hints['height'] ?? 128;

        $srcset = [];
        foreach ($this->sizes as $size) {
            $srcset[] = sprintf("http://placehold.it/%dx%d %dx", $width * $size, $height * $size, $size);
        }

        return [
            'src' => $srcset[0],
            'srcset' => join($srcset, ', '),
        ];
    }
}
