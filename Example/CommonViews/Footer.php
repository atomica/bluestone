<?php

declare(strict_types = 1);

namespace Example\CommonViews;

use Atomica\Bluestone\Contracts\View;

class Footer implements View
{
    public function prepare(array $hints = []) : array
    {
        return $hints;
    }
}
