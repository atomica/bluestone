<html>
<head>
    <title><?= $title ?? 'No Title' ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.2.3/css/bulma.min.css" integrity="sha256-F7gqKszCwmz8vhiti+AICU8dLfIEpxzPVihhhGfbbKg=" crossorigin="anonymous"/>
</head>
<body>
<?= $this->render(\Example\CommonViews\Header::class); ?>
<div class="container">
<?= $this->section('content') ?>
</div>
<?= $this->render(\Example\CommonViews\Footer::class); ?>
</body>
</html>
