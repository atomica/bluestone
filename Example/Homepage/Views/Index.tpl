<?php $this->layout(\Example\CommonViews\Layout::class, ['title' => $title]) ?>

<?php $this->startSection('content'); ?>
<h3>Homepage</h3>
<?= $this->render(\Example\Images\Views\Placeholder::class, ['width' => '128', 'height' => '128']) ?>
<?php $this->endSection('content'); ?>

