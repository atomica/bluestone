<?php

declare(strict_types = 1);

namespace Example\Homepage\Views;

use Atomica\Bluestone\Contracts\View;

class Index implements View
{
    public function prepare(array $hints = []) : array
    {
        return [
            'title' => $hints['title'] ?? 'Default Title',
        ];
    }
}
