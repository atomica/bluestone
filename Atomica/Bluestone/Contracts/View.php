<?php

declare(strict_types = 1);

namespace Atomica\Bluestone\Contracts;

interface View
{
    public function prepare(array $hints = []) : array;
}
