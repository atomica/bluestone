<?php

declare(strict_types = 1);

namespace Atomica\Bluestone\Contracts;

interface Engine
{
    public function render(string $viewName, array $hints = []) : string;
}
