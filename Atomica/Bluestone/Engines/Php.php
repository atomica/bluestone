<?php

declare(strict_types = 1);

namespace Atomica\Bluestone\Engines;

use Atomica\Bluestone\Contracts\Engine;
use Atomica\Bluestone\Contracts\View;
use Atomica\Bluestone\Errors\SectionNotFound;
use Atomica\Bluestone\Errors\TemplateNotFound;
use Atomica\Bluestone\Errors\ViewNotFound;
use Interop\Container\ContainerInterface;

class Php implements Engine
{
    /** @var callable */
    private $__layout = null;

    /** @var string[] */
    private $__sections = [];

    /** @var ContainerInterface */
    private $__container;


    public function __construct(ContainerInterface $container)
    {
        $this->__container = $container;
    }


    public function startSection($name)
    {
        ob_start();
    }


    public function section($name) : string
    {
        return $this->__sections[$name] ?? "";
    }


    public function endSection($name)
    {
        if (!isset($this->__sections[$name])) {
            $this->__sections[$name] = '';
        }
        $this->__sections[$name] .= ob_get_clean();
    }


    public function render(string $viewClassName, array $hints = []) : string
    {
        $copy = clone $this;
        $copy->__layout = null;

        $context = $this->__getContext($viewClassName, $hints);
        $templatePath = $this->__getTemplatePath($viewClassName);

        return $copy->__render($templatePath, $context);
    }


    public function layout(string $viewClassName, array $hints = [])
    {
        $this->__layout = function () use ($viewClassName, $hints) {
            $this->__layout = null;

            return $this->render($viewClassName, $hints);
        };
    }


    private function __render(string $templatePath, array $context = []) : string
    {
        ob_start();
        call_user_func_array(function ($__templatePath, $__context) {
            extract($__context);
            unset($__context);

            include $__templatePath;
        }, [$templatePath, $context]);

        $content = ob_get_clean();

        if ($this->__layout) {
            $content = call_user_func($this->__layout);
        }

        return $content;
    }


    private function __getContext(string $viewClassName, array $hints) : array
    {
        /** @var View $view */
        $view = $this->__container->get($viewClassName);
        if (!$view) {
            throw new ViewNotFound($viewClassName);
        }

        return $view->prepare($hints);
    }


    private function __getTemplatePath(string $viewClassName) : string
    {
        $reflectedView = new \ReflectionClass($viewClassName);

        $templatePath = str_replace('.php', '.tpl', $reflectedView->getFileName());

        if (!is_readable($templatePath)) {
            throw new TemplateNotFound($templatePath, $viewClassName);
        }

        return $templatePath;
    }
}
