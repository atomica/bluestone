<?php

namespace Atomica\Bluestone\Engines;

use Atomica\Bluestone\Errors\SectionNotFound;
use Atomica\Bluestone\Errors\TemplateNotFound;
use Atomica\Bluestone\Errors\ViewNotFound;
use Example\CommonViews\Footer;
use Example\CommonViews\Header;
use Example\CommonViews\Layout;
use Example\Fixtures\MissingSection;
use Example\Fixtures\MissingTemplate;
use Example\Fixtures\StackingSections;
use Example\Homepage\Views\Index;
use Example\Images\Views\Placeholder;
use Interop\Container\ContainerInterface;
use PhpSpec\ObjectBehavior;

/**
 * @mixin Php
 */
class PhpSpec extends ObjectBehavior
{
    function let(ContainerInterface $container)
    {
        $this->beConstructedWith($container);
    }


    function it_throws_an_exception_if_the_view_is_not_found()
    {
        $this->shouldThrow(ViewNotFound::class)->during('render', ['Example\Fixtures\MissingView']);
    }


    function it_throws_an_exception_if_the_template_is_not_found(ContainerInterface $container)
    {
        $container->get(MissingTemplate::class)->willReturn(new MissingTemplate());
        $this->shouldThrow(TemplateNotFound::class)->during('render', [MissingTemplate::class]);
    }

    function it_returns_an_empty_string_if_a_section_does_not_exist(ContainerInterface $container)
    {
        $container->get(MissingSection::class)->willReturn(new MissingSection());
        $this->render(MissingSection::class)->shouldContain('__');
    }

    function it_appends_sections(ContainerInterface $container)
    {
        $container->get(StackingSections::class)->willReturn(new StackingSections());
        $this->render(StackingSections::class)->shouldContain("hello\nagain");
    }


    function it_renders_template_files_with_context(ContainerInterface $container)
    {
        $container->get(Index::class)->willReturn(new Index());
        $container->get(Placeholder::class)->willReturn(new Placeholder());
        $container->get(Layout::class)->willReturn(new Layout());
        $container->get(Header::class)->willReturn(new Header());
        $container->get(Footer::class)->willReturn(new Footer());

        $hints = ['title' => 'specialTitle'];
        $this->render(Index::class, $hints)->shouldContain('specialTitle');
    }
}
