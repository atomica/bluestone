<?php

namespace Atomica\Bluestone;

use Atomica\Bluestone\Contracts\Engine;
use PhpSpec\ObjectBehavior;

/**
 * @mixin Bluestone
 */
class BluestoneSpec extends ObjectBehavior
{
    function let(Engine $engine)
    {
        $this->beConstructedWith($engine);
    }


    function it_passes_through_to_an_engine(Engine $engine)
    {
        $view = 'View';
        $hints = [];
        $engine->render($view, $hints)->willReturn('rendered');

        $this->render($view, $hints)->shouldBe('rendered');
    }
}
