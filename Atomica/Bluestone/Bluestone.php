<?php

declare(strict_types = 1);

namespace Atomica\Bluestone;

use Atomica\Bluestone\Contracts\Engine;

class Bluestone
{
    /** @var Engine */
    private $engine;


    public function __construct(Engine $engine)
    {
        $this->engine = $engine;
    }


    public function render(string $viewName, array $hints = []) : string
    {
        return $this->engine->render($viewName, $hints);
    }
}
