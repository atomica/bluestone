<?php declare(strict_types = 1);

namespace Atomica\Bluestone\Errors;

use Exception;

class TemplateNotFound extends Exception
{
    public function __construct(string $template, string $view)
    {
        $message = sprintf("Template '%s' not found for view '%s'", $template, $view);
        parent::__construct($message);
    }
}
