<?php

declare(strict_types = 1);

namespace Atomica\Bluestone\Errors;

class SectionNotFound extends \Exception
{
}
